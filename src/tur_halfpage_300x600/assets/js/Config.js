import Content from '../../../common/assets/js/modules/Content';


const config = {
  modules: {
    content: Content
  },
  adSize: {
    x: 0,
    y: 0,
    width: 300,
    height: 600
  },
  contentSize: {
    x: 0,
    y: 0,
    width: 300,
    height: 532
  },
  videoSize: {
    x: 0,
    y: 0,
    width: 300,
    height: 532
  },
  video: {
    showProgress: false
  }
};
export default config;