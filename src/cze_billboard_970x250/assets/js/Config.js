import Content from '../../../common/assets/js/modules/Content';

const config = {
  modules: {
    content: Content
  },
  adSize: {
    x: 0,
    y: 0,
    width: 970,
    height: 250
  },
  contentSize: {
    x: 0,
    y: 0,
    width: 790,
    height: 250
  },
  videoSize: {
    x: 0,
    y: 0,
    width: 790,
    height: 250
  },
  video: {
    showProgress: false
  }
};
export default config;