'use strict';

import Dispatcher from '../../../common/assets/js/helper/dispatcher';
import Layer from '../../../common/assets/js/modules/components/Layer';
import Button from '../../../common/assets/js/modules/components/Button';

export default class Content {

  constructor(props) {
    this.props = (props || {});
  }

  init() {

    this.currentIndex = 0;

    this._setupDom();
    this._addListener();

    this.connectLayer = new Layer({el: this.connectLayerEl});
    this.ePerformLayer = new Layer({el: this.ePerformLayeEl});

    this.connectBtn = new Button({el: this.connectBtnEl});
    this.ePerformBtn = new Button({el: this.ePerformBtnEl});
  }

  _setupDom() {
    this.el = document.getElementById('content-container');
    this.slideContent = document.getElementById('slide-container');
    this.spinner = document.getElementById('pag-spinner');
    this.h1El = document.querySelector('.copy-container h1');
    this.h2El = document.querySelector('.copy-container .subhead');
    this.footer = document.querySelector('footer');
    this.connectBtnEl = document.getElementById('connect-btn');
    this.ePerformBtnEl = document.getElementById('e-perform-btn');
    this.connectLayerEl = document.getElementById('connect-layer');
    this.ePerformLayeEl = document.getElementById('e-perform-layer');
    this.ctaBtnEl = document.getElementById('cta-btn');
    this.restartBtnEl = document.getElementById('restart-btn');
    this.navigationEl = document.querySelector('nav');
    this.leftBtnEl = document.querySelector('nav .left');
    this.rightBtnEl = document.querySelector('nav .right');
  }

  _addListener() {
    this.el.addEventListener('click', ::this.onBtnClick, false);
    this.h1El.addEventListener('click', ::this.onBtnClick, false);
    this.connectBtnEl.addEventListener('click', ::this.onBtnClick, false);
    this.ePerformBtnEl.addEventListener('click', ::this.onBtnClick, false);
    this.restartBtnEl.addEventListener('click', ::this.onBtnClick, false);
    this.leftBtnEl.addEventListener('click', ::this.onBtnClick, false);
    this.rightBtnEl.addEventListener('click', ::this.onBtnClick, false);
    if (this.logoArea) {
      this.logoArea.addEventListener('click', ::this.onBtnClick, false);
    }
    Enabler.addEventListener(studio.events.StudioEvent.EXIT, ::this.onExit);
  }

  _slideLeft() {
    this.currentIndex++;
    TweenLite.to(this.navigationEl, 0.2, {autoAlpha: 0});
    TweenLite.to(this.slideContent, 1, {x: this.currentIndex * this.props.contentSize.width, ease: Expo.easeInOut, onComplete: ::this.onSlideComplete})
  }

  _slideRight() {
    this.currentIndex--;
    TweenLite.to(this.navigationEl, 0.3, {autoAlpha: 0});
    TweenLite.to(this.slideContent, 1, {x: this.currentIndex * this.props.contentSize.width, ease: Expo.easeInOut, onComplete: ::this.onSlideComplete})
  }

  onSlideComplete() {
    switch (this.currentIndex) {
      case -1:
        this.rightBtnEl.classList.add('hidden');
        this.leftBtnEl.disabled = false;
        break;
      case 0:
        this.leftBtnEl.classList.remove('hidden');
        this.rightBtnEl.classList.remove('hidden');
        this.leftBtnEl.disabled = false;
        this.rightBtnEl.disabled = false;
        break;
      case 1:
        this.leftBtnEl.classList.add('hidden');
        this.rightBtnEl.disabled = false;
        break;
      default:
    }
    this.navigationEl.disabled = false;
    TweenLite.to(this.navigationEl, 0.7, {autoAlpha: 1});
  }

  transitionIn(duration = 1) {
    // let duration = 1;
    let ease = Expo.easeOut;

    this.tl = new TimelineLite();
    // this.tl.timeScale(3);
    this.tl
      .fromTo(this.el, duration, {autoAlpha: 0}, {autoAlpha: 1, ease: Quad.easeOut}, "+=0")
      .from(this.h1El, duration, {x: 10, autoAlpha: 0, ease: ease}, "+=0.5")
      .from(this.h2El, duration, {x: 10, autoAlpha: 0, ease: ease}, "-=0.8")
      .from(this.footer, duration, {autoAlpha: 0, ease: ease}, "-=0.5")
      .from(this.connectBtnEl, duration, {scale: 0.3, autoAlpha: 0, rotation: 180, ease: ease}, "-=0.3")
      .from(this.ePerformBtnEl, duration, {scale: 0.3, autoAlpha: 0, rotation: 360, ease: ease}, "-=0.5")
      .to(this.ctaBtnEl, duration, {autoAlpha: 1, ease: ease}, "-=0")
  }

  transitionOut() {
    this.tl = new TimelineLite();
    this.tl.to(this.el, 0.5, {autoAlpha: 0, ease: Quad.easeOut, onComplete: ::this.onTransitionOutComplete}, "+=0")
  }

  onBtnClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    switch (ev.currentTarget) {
      case this.el:
        Enabler.exit('Exit Background');
        break;
      case this.h1El:
        Enabler.exit('Exit CTA');
        break;
      case this.connectBtnEl:
        this.connectLayer.transitionIn();
        Enabler.counter('Click on Connect');
        break;
      case this.ePerformBtnEl:
        this.ePerformLayer.transitionIn();
        Enabler.counter('Click on E-Performance');
        break;
      case this.restartBtnEl:
        this.transitionOut();
        Enabler.counter('Video restarted');
        break;
      case this.leftBtnEl:
        this.leftBtnEl.disabled = true;
        this.rightBtnEl.disabled = true;
        this._slideLeft();
        break;
      case this.rightBtnEl:
        this.leftBtnEl.disabled = true;
        this.rightBtnEl.disabled = true;
        this._slideRight();
        break;
      default:
    }
  }

  onTransitionOutComplete() {
    Dispatcher.dispatch('replay', {});
  }

  onExit() {
    this.transitionIn(0);
  }
}