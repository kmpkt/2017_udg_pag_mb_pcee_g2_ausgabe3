'use strict';

import Application from '../../../common/assets/js/modules/Application';
import Config from './Config';

creative.app = new Application(Config);
creative.app.init();
