import Content from './Content';


const config = {
  modules: {
    content: Content
  },
  adSize: {
    x: 0,
    y: 0,
    width: 300,
    height: 250
  },
  contentSize: {
    x: 0,
    y: 68,
    width: 300,
    height: 182
  },
  videoSize: {
    x: 0,
    y: 0,
    width: 300,
    height: 182
  },
  video: {
    showProgress: false
  }
};
export default config;