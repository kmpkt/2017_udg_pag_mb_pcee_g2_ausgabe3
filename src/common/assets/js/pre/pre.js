var creative = creative || {};
creative.dc = {

  init: function(cb) {

    if (Enabler.isInitialized()) enablerInitiated();
    else Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitiated);

    function enablerInitiated() {
      console.log('DC State: INIT COMPLETE');
      if (cb.onInit)
      {
        cb.onInit();
      }
      if (Enabler.isPageLoaded()) pageLoaded();
      else Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, pageLoaded);
    }

    function pageLoaded() {
      console.log('DC State: POLITE LOAD COMPLETE');
      if (cb.onPolite)
      {
        cb.onPolite();
      }
      if (Enabler.isVisible()) onVisible();
      else Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, onVisible);
    }

    function onVisible() {
      console.log('DC State: VISIBLE');
      if (cb.onVisible)
      {
        cb.onVisible();
      }
    }
  }
};

// IE console fix
if (!window.console) {
  window.console = {
    log: function() {
    }
  };
}

///////////////////////
// E X A M P L E  U S E
/*

 creative.dc.init({
 onInit: function() {

 },
 onPolite: function() {

 },
 onVisible: function() {

 }
 });

 */
