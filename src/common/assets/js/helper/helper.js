'use strict';

var HelperModule = function() {
  //creative.imageCache = {};
  //this.autoSizeClass = 'auto-fit-text';
};

function imageLoader (images, callback, scope) {
  let numImages = images.length;
  let loadedImages = 0;
  let img = null;
  for(let i = 0; i < numImages; ++i) {
    img = document.createElement('img');
    img.src = img.shortSrc = images[i];
    img.onload = function() {
      loadedImages++;
      creative.imageCache[this.shortSrc] = this;
      if(loadedImages == numImages) {
        if (scope) {
          callback.call(scope);
        } else {
          callback();
        }
      }
    };
  }
};

function autoSizeText(className = 'auto-fit-text') {

  let el, elements,  _len, _results;
  elements = document.getElementsByClassName(className);
  //console.log(elements);
  if (elements.length < 0)
  {
    return;
  }

  _results = [];
  for (let i = 0, _len = elements.length; i < _len; i++)
  {
    el = elements[i];
    _results.push((function(el) {
      let resizeText, _results1, elNewFontSize;
      resizeText = function() {
        elNewFontSize = (parseInt(getComputedStyle(el)['font-size'].slice(0, -2)) - 1) + 'px';
        return el.style.fontSize = elNewFontSize;
      };
      _results1 = [];
      while (el.scrollHeight > el.offsetHeight)
      {
        _results1.push(resizeText());
      }
      return _results1;
    })(el));
  }
  return _results;
};

/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function isIE() {
  let ua = window.navigator.userAgent;

  // Test values; Uncomment to check result …

  // IE 10
  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

  // IE 11
  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

  // IE 12 / Spartan
  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

  // Edge (IE 12+)
  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  let msie = ua.indexOf('MSIE ');
  if (msie > 0)
  {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  let trident = ua.indexOf('Trident/');
  if (trident > 0)
  {
    // IE 11 => return version number
    let rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  let edge = ua.indexOf('Edge/');
  if (edge > 0)
  {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}

function isIOS() {

  let iDevices = [
    'iPad Simulator',
    'iPhone Simulator',
    'iPod Simulator',
    'iPad',
    'iPhone',
    'iPod'
  ];

  if (!!navigator.platform) {
    while (iDevices.length) {
      if (navigator.platform === iDevices.pop()){ return true; }
    }
  }

  return false;
}

export { autoSizeText, imageLoader, isIE, isIOS };