'use strict';

import Button from './Button';

export default class Layer {

  constructor(props) {
    this.props = (props || {});
    this.el = this.props.el;
    this.init();
  }

  init() {
    this._setupDom();
    this._addListener();

    this.closeBtn = new Button({el:this.closeBtnEl});
  }

  _setupDom() {
    this.closeBtnEl = this.el.querySelector('.close-btn');
  }

  _addListener() {
    this.closeBtnEl.addEventListener('click', ::this.onBtnClick, false);
    Enabler.addEventListener(studio.events.StudioEvent.EXIT, ::this.onExit);
  }

  transitionIn() {
    TweenLite.to(this.el, 0.9, {autoAlpha: 1, ease: Expo.easeOut});
  }

  transitionOut() {
    TweenLite.to(this.el, 0.3, {autoAlpha: 0, ease: Expo.easeOut});
  }

  onBtnClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    this.transitionOut();
  }

  onExit() {
  }

}