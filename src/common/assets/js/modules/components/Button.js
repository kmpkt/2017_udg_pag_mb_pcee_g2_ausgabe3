'use strict';

export default class Button {

  constructor(props) {
    this.props = (props || {});
    this.el = this.props.el;
    this.init();
  }

  init() {
    this._setupDom();
    this._addListener();
  }

  _setupDom() {
  }

  _addListener() {
    this.el.addEventListener('mouseenter', ::this.onOver, false);
    this.el.addEventListener('mouseleave', ::this.onOut, false);

    Enabler.addEventListener(studio.events.StudioEvent.EXIT, ::this.onExit);
  }

  transitionIn() {
    TweenLite.to(this.el, 0.9, {autoAlpha: 1, ease: Expo.easeOut});
  }

  transitionOut() {
  }

  onOver(ev) {
    TweenLite.to(this.el, 0.7, {rotation: 45, ease: Expo.easeOut});
  }

  onOut(ev) {
    TweenLite.to(this.el, 0.5, {rotation: 0, ease: Expo.easeOut});
  }

  onExit() {
  }

}