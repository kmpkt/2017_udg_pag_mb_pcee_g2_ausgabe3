'use strict';

export default class Logo {

  constructor(props) {
    this.props = (props || {});
  }

  init() {
    this._setupDom();
    this._addListener();

  }

  _setupDom() {
    this.el = document.querySelector('.logo-container');
  }

  _addListener() {
    this.el.addEventListener('click', ::this.onBtnClick, false);
    Enabler.addEventListener(studio.events.StudioEvent.EXIT, ::this.onExit);
  }

  transitionIn() {
    TweenLite.to(this.el, 0.9, {autoAlpha: 1, ease: Expo.easeOut});
  }

  transitionOut() {
    TweenLite.to(this.el, 0.3, {autoAlpha: 0, ease: Expo.easeOut});
  }

  onBtnClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    Enabler.exit('Exit Logo');
  }

  onExit() {
  }

}