'use strict';

import Dispatcher from '../helper/dispatcher';
import device from '../../../../common/assets/js/helper/device';
import modernizr from "../../../../common/assets/js/helper/modernizr";
import { isIOS } from '../../../../common/assets/js/helper/helper';


export default class VideoModule {

  static VERSION = '1.15';

  constructor(props) {
    this.props = (props || {});
  }

  init() {
    this._setupDom();
    this._addListener();
    // Remove player controls
    this.video.removeAttribute('controls');
    // Mute video on start
    this.video.muted = true;

    // If device is mobile, hide VideoPoster
    if (Modernizr.touchevents || device.mobile()) {
      this.videoPoster.classList.remove('hidden');
    }

    // Add video metrics to the HTML5 video elements by loading in video module, and assigning to videos.
    Enabler.loadModule(studio.module.ModuleId.VIDEO, function() {
      studio.video.Reporter.attach('Video Report', this.video);
    }.bind(this));
  }

  _setupDom() {
    this.video = document.getElementById('video1');
    this.playButton = document.getElementById('play-btn');
    this.pauseButton = document.getElementById('pause-btn');
    this.muteButton = document.getElementById('sound-mute-btn');
    this.unmuteButton = document.getElementById('sound-unmute-btn');
    this.videoContainer = document.getElementById('video-container');
    this.audioUI = document.getElementById('audio-ui');
    this.videoUI = document.getElementById('video-ui');
    this.progressUI = document.getElementById('progress-ui');
    this.progressBar = document.getElementById('progress-bar');
    this.videoPoster = document.getElementById('video-poster');
  }

  _addListener() {
    this.video.addEventListener('play', ev => this.onVideoPlay(ev), false);
    this.video.addEventListener('ended', ev => this.onVideoComplete(ev), false);
    this.playButton.addEventListener('click', ev => this.onBtnClick(ev), false);
    this.pauseButton.addEventListener('click', ev => this.onBtnClick(ev), false);
    this.muteButton.addEventListener('click', ev => this.onBtnClick(ev), false);
    this.unmuteButton.addEventListener('click', ev => this.onBtnClick(ev), false);
    this.videoPoster.addEventListener('click', ev => this.onBtnClick(ev), false);

    Enabler.addEventListener(studio.events.StudioEvent.EXIT, ev => this.onExit(ev));
  }

  _updateProgress() {
    try {
      var duration = this.video.duration;
      var currentTime = this.video.currentTime;
      var progress = currentTime / duration * 100;
      this.progressBar.style.width = progress + '%';
    }
    catch (e) {
    }
    this.watchId = requestAnimationFrame(::this._updateProgress);
  }

  _startWatch() {
    if (this.props.video.showProgress && !this.watchId) {
      this.progressUI.classList.remove('hidden');
      this.watchId = requestAnimationFrame(::this._updateProgress);
    }
  }

  _stopWatch() {
    if (this.watchId) {
      cancelAnimationFrame(this.watchId);
      this.watchId = null;
    }
  }

  _alignUI() {
    var videoX = (this.props.videoSize.width - this.playButton.offsetWidth) / 2 - this.videoUI.offsetLeft;
    var videoY = (this.props.videoSize.height - this.playButton.offsetHeight) / 2 - this.videoUI.offsetTop;
    this.playButton.style.left = videoX + 'px';
    this.playButton.style.top = videoY + 'px';
  }

  _swap(itemShow, itemHide) {
    itemShow.classList.remove('hidden');
    itemHide.classList.add('hidden');
  }

  restart() {
    this._startWatch();
    this.videoContainer.classList.add('hidden');
    this.video.currentTime = 0;
    this.video.play();
    this._swap(this.pauseButton, this.playButton);
  }

  transitionIn() {
    this._startWatch();
    this.videoContainer.classList.remove('hidden');

    let dur = 0.3;
    let tl = new TimelineLite();
    if (Modernizr.touchevents || device.mobile()) {
      this._swap(this.playButton, this.pauseButton);
      this._alignUI();

      tl.set(this.playButton, {autoAlpha: 0})
        .set(this.audioUI, {autoAlpha: 0})
        .to(this.playButton, dur, {autoAlpha: 1});

    }
    else {
      tl.set(this.pauseButton, {autoAlpha: 0})
        .set(this.audioUI, {autoAlpha: 0})
        .to(this.pauseButton, dur, {autoAlpha: 1})
        .to(this.audioUI, dur, {autoAlpha: 1}, "-=0.15")
    }

  }

  transitionOut(duration = 0.15) {
    let tl = new TimelineLite();
    tl.to(this.audioUI, duration, {autoAlpha: 0})
      .to(this.pauseButton, duration, {autoAlpha: 0}, "-=0.07")
      .set(this.videoContainer, {css: {className: '+=hidden'}});
  }

  play() {
    this.videoPoster.classList.add('hidden');
    this.video.play();
    this._swap(this.pauseButton, this.playButton);
  }

  stop() {
    this.video.pause();
    this._stopWatch();
    this._swap(this.playButton, this.pauseButton);
  }

  rewind() {
    this.video.currentTime = 0;
  }

  getCurrentProgress() {
    let progress = -1;
    try {
      let duration = this.video.duration;
      let currentTime = this.video.currentTime;
      progress = currentTime / duration;
    }
    catch (e) {
    }
    return progress;
  }

  onBtnClick(ev) {
    ev.preventDefault();
    switch (ev.target) {
      case this.playButton:
        this.video.play();
        this._swap(this.pauseButton, this.playButton);
        break;
      case this.pauseButton:
        this.video.pause();
        this._swap(this.playButton, this.pauseButton);
        this._alignUI();
        break;
      case this.muteButton:
        this.video.muted = true;
        this._swap(this.unmuteButton, this.muteButton);
        break;
      case this.unmuteButton:
        this.video.muted = false;
        this._swap(this.muteButton, this.unmuteButton);
        break;
      case this.videoPoster:
        this.videoPoster.style.display = 'none';
        this.play();
        break;
      default:
    }
  }

  onVideoPlay(ev) {
    this.videoPoster.classList.add('hidden');
    // If mobile device is not Android, show audioUI
    if ((Modernizr.touchevents || device.mobile()) && !isIOS()) {
      TweenLite.set(this.audioUI, {autoAlpha: 1});
    }
  }

  onVideoComplete(ev) {
    //this.videoContainer.style.display = 'none';
    Dispatcher.dispatch('videoComplete', {});
    this._stopWatch();
    this.video.pause();
  }

  onExit(ev) {
    this._stopWatch();
    this.video.pause();
    this.transitionOut(0);
  }
}