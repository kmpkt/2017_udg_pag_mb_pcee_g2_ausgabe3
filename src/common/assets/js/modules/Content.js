'use strict';

import Dispatcher from '../helper/dispatcher';
import Layer from './components/Layer';
import Button from './components/Button';

export default class Content {

  constructor(props) {
    this.props = (props || {});
  }

  init() {
    this._setupDom();
    this._addListener();

    this.connectLayer = new Layer({el: this.connectLayerEl});
    this.ePerformLayer = new Layer({el: this.ePerformLayeEl});

    this.connectBtn = new Button({el: this.connectBtnEl});
    this.ePerformBtn = new Button({el: this.ePerformBtnEl});
  }

  _setupDom() {
    this.el = document.getElementById('content-container');
    this.spinner = document.getElementById('pag-spinner');
    this.h1El = document.querySelector('.copy-container h1');
    this.h2El = document.querySelector('.copy-container .subhead');
    this.footer = document.querySelector('footer');
    this.connectBtnEl = document.getElementById('connect-btn');
    this.ePerformBtnEl = document.getElementById('e-perform-btn');
    this.connectLayerEl = document.getElementById('connect-layer');
    this.ePerformLayeEl = document.getElementById('e-perform-layer');
    this.ctaBtnEl = document.getElementById('cta-btn');
    this.restartBtnEl = document.getElementById('restart-btn');

  }

  _addListener() {
    this.el.addEventListener('click', ::this.onBtnClick, false);
    this.connectBtnEl.addEventListener('click', ::this.onBtnClick, false);
    this.ePerformBtnEl.addEventListener('click', ::this.onBtnClick, false);
    this.restartBtnEl.addEventListener('click', ::this.onBtnClick, false);
    if (this.logoArea) {
      this.logoArea.addEventListener('click', ::this.onBtnClick, false);
    }
    Enabler.addEventListener(studio.events.StudioEvent.EXIT, ::this.onExit);
  }

  transitionIn(duration = 1) {
    // let duration = 1;
    let ease = Expo.easeOut;

    this.tl = new TimelineLite();
    // this.tl.timeScale(3);
    this.tl
      .fromTo(this.el, duration, {autoAlpha: 0}, {autoAlpha: 1, ease: Quad.easeOut}, "+=0")
      .from(this.h1El, duration, {x: 10, autoAlpha: 0, ease: ease}, "+=0.5")
      .from(this.h2El, duration, {x: 10, autoAlpha: 0, ease: ease}, "-=0.8")
      .from(this.footer, duration, {autoAlpha: 0, ease: ease}, "-=0.5")
      .from(this.connectBtnEl, duration, {scale: 0.3, autoAlpha: 0, rotation: 180, ease: ease}, "-=0.3")
      .from(this.ePerformBtnEl, duration, {scale: 0.3, autoAlpha: 0, rotation: 360, ease: ease}, "-=0.5")
      .to(this.ctaBtnEl, duration, {autoAlpha: 1, ease: ease}, "-=0");

    // Add debug scrubber if script is loaded
    if (creative.scrubber) {
      creative.scrubber({'timeline': this.tl});
    }
  }

  transitionOut() {
    this.tl = new TimelineLite();
    this.tl.to(this.el, 0.5, {autoAlpha: 0, ease: Quad.easeOut, onComplete: ::this.onTransitionOutComplete}, "+=0")
  }

  onBtnClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    switch (ev.currentTarget) {
      case this.el:
        Enabler.exit('Exit Background');
        break;
      case this.connectBtnEl:
        this.connectLayer.transitionIn();
        Enabler.counter('Click on Connect');
        break;
      case this.ePerformBtnEl:
        this.ePerformLayer.transitionIn();
        Enabler.counter('Click on E-Performance');
        break;
      case this.restartBtnEl:
        this.transitionOut();
        Enabler.counter('Video restarted');
        break;
      default:
    }
  }

  onTransitionOutComplete() {
    TweenLite.set(this.ctaBtnEl, {autoAlpha: 0});
    Dispatcher.dispatch('replay', {});
  }

  onExit() {
    this.transitionIn(0);
  }
}