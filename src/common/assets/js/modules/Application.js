'use strict';

import VideoModule from './Video';
import LogoComponent from './components/Logo';
import Dispatcher from '../helper/dispatcher';
import device from '../../../../common/assets/js/helper/device';

const STATE_VIDEO = 'stateVideo';
const STATE_ENDSCREEN = 'stateEndscreen';

let state = STATE_VIDEO;

export default class Application {

  constructor(props) {
    this.props = (props || {});
  }

  init() {
    // Register events
    Dispatcher.register('videoComplete', ev => this.onVideoComplete(ev));
    Dispatcher.register('replay', ev => this.onContentComplete(ev));
    // Dispatcher.register('logoClicked', ev => this.onEndscreenComplete(ev));

    this._setupDom();
    this._addListener();
    console.log('init');
    this.show();
    /**
     * Testing
     */
    // this.onVideoComplete();
  }

  _setupDom() {
    this.mainEl = document.getElementById('main-container');
    this.fallbackEl = document.getElementById('fallback');
    this.politeEl = document.getElementById('polite');
    this.ctaBtnEl = document.getElementById('cta-btn');
    this.videoEl = document.getElementById('video1');
    this.logo = new LogoComponent(this.props);
    this.logo.init();
    this.video = new VideoModule(this.props);
    this.video.init();
    this.content = new this.props.modules.content(this.props);
    this.content.init();
  }

  _addListener() {
    this.ctaBtnEl.addEventListener('click', ::this.onBtnClick, false);
    this.videoEl.addEventListener('click', ::this.onBtnClick, false);
    this.videoEl.addEventListener('webkitendfullscreen', ::this.onWebkitEndFullscreen);
    Enabler.addEventListener(studio.events.StudioEvent.EXIT, ::this.onExit);
  }

  _showContent() {
    state = STATE_ENDSCREEN;
    this.content.transitionIn();
    this.video.transitionOut();
  }

  show() {
    // Remove loader
    var spinner = document.getElementById('pag-spinner');
    spinner.parentNode.removeChild(spinner);
    this.politeEl.style.visibility = 'visible';
    this.logo.transitionIn();
    this.video.transitionIn();

    if (Modernizr.touchevents || device.mobile()) {
    }
    else {
      this.video.play();
    }
  }

  showVideoModule() {
    this.video.transitionIn();
  }

  onVideoComplete() {
    if (!device.iphone()) {
      this._showContent();
    }
  }

  /**
   * iPhone only
   * Show content if ios fullscreen video has been closed before end
   */
  onWebkitEndFullscreen() {
    // console.log(device.iphone(), this.video.getCurrentProgress());
    if (device.iphone()) {
      this._showContent();
    }
  }

  onContentComplete() {
    state = STATE_VIDEO;
    this.video.rewind();
    this.video.play();
    // Prevent last video frame flicker
    TweenLite.delayedCall(0.1, ev => this.showVideoModule(ev));
  }

  onBtnClick(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    switch (ev.currentTarget) {
      case this.ctaBtnEl:
        Enabler.exit('Exit CTA');
        break;
        case this.videoEl:
        Enabler.exit('Exit Video');
        break;
      case this.fallbackEl:
        Enabler.exit('Exit Background');
        break;
      default:
    }
  }

  onExit() {
    if (state === STATE_VIDEO) {
      this.video.transitionOut();
      state = STATE_ENDSCREEN;
    }
  }
}