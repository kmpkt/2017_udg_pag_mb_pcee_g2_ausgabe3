var creative = window.creative || {};
creative = {

  createElement: function(_settings) {

    // default settings
    var settings = _settings || {};
    settings.type = settings.type || "div";
    settings.position = settings.position || "absolute";

    var element = document.createElement(settings.type);

    switch (settings.type) {
      case "canvas" :
        element.width = settings.width;
        element.height = settings.height;
        break;
      case "video" :
        if (settings.autoplay) element.autoplay = settings.autoplay;
        if (settings.loop) element.loop = settings.loop;
        if (settings.controls) element.controls = settings.controls;
        if (settings.muted) element.muted = settings.muted;
        if (settings.poster) element.poster = settings.poster;
        if (settings.preload) element.preload = settings.preload;
        break;
    }

    // set video sources
    if (settings.sources) {
      // loop through sources array
      var sources = settings.sources;
      for (var i = 0; i < sources.length; i++) {
        // create source tag
        var sourceTag = document.createElement("source");
        sourceTag.src = sources[i].url;
        sourceTag.type = sources[i].type;
        element.appendChild(sourceTag);
      }
    }

    // handle image
    if (settings.backgroundImage) {

      element.style.backgroundSize = settings.backgroundSize || "contain";
      element.style.backgroundRepeat = settings.backgroundRepeat || "no-repeat";

      if (Object.prototype.toString.call(settings.backgroundImage) === "[object Array]") {
        element.images = settings.backgroundImage;
        element.sequences = {};
        element.currentFrame = 1;
        element.addSequence = function(label, frames) {
          this.sequences[label] = frames;
        };
        element.playSequence = function(label) {
          var _this = this;
          var sequence = this.sequences[label];
          for (var i = 0; i < sequence.length; ++i) {
            TweenLite.delayedCall((1 / 24) * i, function(frame) {
              _this.gotoAndStop(frame);
            }, [sequence[i]]);
          }
        };
        element.gotoAndStop = function(frame) {
          this.currentFrame = frame;
          this.style.backgroundImage = "url(" + this.images[frame - 1] + ")";
        };

        for (var i = 0; i < element.images.length; ++i) {
          loadImg(element.images[i], i === 0);
        }
      } else {
        loadImg(settings.backgroundImage, true);
      }

    } else {
      applySettings();
    }

    function applySettings() {
      if (settings.id) element.id = settings.id;

      if (settings.parent) settings.parent.appendChild(element);
      if (settings.innerHTML) element.innerHTML = settings.innerHTML;
      delete settings.innerHTML;
      delete settings.retina;
      delete settings.parent;
      delete settings.id;
      delete settings.type;
      delete settings.autoplay;
      delete settings.loop;
      delete settings.controls;
      delete settings.muted;
      delete settings.poster;
      delete settings.preload;
      delete settings.sources;

      TweenLite.set(element, settings);
    }

    function loadImg(src, doSetImage) {
      var img = porsche.image_cache[src];
      if (img) { // if preloaded
        if (doSetImage) setImage.apply(img);
      } else {
        console.log("Image " + src + " has not been preloaded.");
        img = document.createElement("img");
        img.src = src;
        if (doSetImage) img.onload = setImage;
        porsche.image_cache[src] = img;
      }
    }

    function setImage() {
      var isSVG = this.src.slice(-4) == ".svg";
      if (isSVG) document.body.appendChild(this); // IE fix
      settings.width = settings.width || Math.round(settings.retina ? this.width / 2 : this.width);
      settings.height = settings.height || Math.round(settings.retina ? this.height / 2 : this.height);
      settings.backgroundImage = "url(" + this.src + ")";
      applySettings();
      if (isSVG) document.body.removeChild(this); // IE fix
    }

    // functions
    element.appendChildren = function(children) {
      for (var i = 0; i < children.length; ++i) this.appendChild(children[i]);
      return this;
    };
    element.set = function(settings) {
      TweenLite.set(this, settings);
      return this;
    };
    element.to = function(time, settings) {
      TweenLite.to(this, time, settings);
      return this;
    };
    element.from = function(time, settings) {
      TweenLite.from(this, time, settings);
      return this;
    };
    element.fromTo = function(time, from, to) {
      TweenLite.fromTo(this, time, from, to);
      return this;
    };
    element.get = function(property) {
      return ((this._gsTransform && this._gsTransform[property]) || (this._gsTransform && this._gsTransform[property] === 0)) ? this._gsTransform[property] : (this.style[property].slice(-2) == "px") ? parseFloat(this.style[property]) : this.style[property];
    };
    element.centerHorizontal = function() {
      TweenLite.set(this, {left: 0, right: 0, marginLeft: "auto", marginRight: "auto"});
      return this;
    };
    element.centerVertical = function() {
      TweenLite.set(this, {top: 0, bottom: 0, marginTop: "auto", marginBottom: "auto"});
      return this;
    };
    element.center = function() {
      TweenLite.set(this, {top: 0, left: 0, right: 0, bottom: 0, margin: "auto"});
      return this;
    };

    return element;
  },

  preloadImages: function(images, callback, scope) {
    var numImages = images.length;
    var loadedImages = 0;
    var img = null;
    for (var i = 0; i < numImages; ++i) {
      img = document.createElement("img");
      img.src = img.shortSrc = images[i];
      img.onload = function() {
        loadedImages++;
        porsche.image_cache[this.shortSrc] = this;
        if (loadedImages == numImages) {
          if (scope) {
            callback.call(scope);
          } else {
            callback();
          }
        }
      };
    }
  },
  include: function(script, callback) {
    console.log("#include '" + script + "'");
    var include_script = document.createElement('script');
    include_script.type = "text/javascript";
    include_script.src = script;
    document.head.appendChild(include_script);
    include_script.onload = callback ? callback : function() {
      console.log("Resource loaded: " + include_script.src);
    };
    include_script.onerror = function() {
      console.log("Oops! Could not load resource '" + script + "'");
    };
  }

};

creative.scrubber = function(timelines) {

  console.log("=== Banner timeline enabled ===");

  creative.scrubberController = function(tl) {

    var labels = [];
    var labelNames = [];
    var i = 0;

    //create main container
    var mainContainer = this.mainContainer = creative.createElement({id: "toggleContainer", width: 530, height: 80, position: "fixed", bottom: 0, left: 30});

    //create toggle container
    var toggleContainer = this.toggleContainer = creative.createElement({id: "toggleContainer", width: 560, height: 50, top: 20, paddingTop: "5px", paddingLeft: "5px", overflow: "hidden"});

    //var handle = this.handle = creative.createElement({innerHTML: "||", left: -10, width:8, height: 20, color:"#ef5c63", lineHeight: "21px", position: "absolute"});

    //create name
    var name = this.name = creative.createElement({innerHTML: "timeline", color: "#ef5c63", fontFamily: "verdana", userSelect: "none", cursor: "pointer"});

    //create container
    var container = creative.createElement({width: 523, height: 30, float: "left", position: "relative"});

    //create background
    var bg = creative.createElement({width: 520, height: 10, background: "#36b1c5", borderRadius: 10, position: "absolute", boxShadow: "0 0 0 3px #86c8d8", marginLeft: -10, parent: container});

    //create time indicator
    var time = creative.createElement({innerHTML: "0", left: 535, top: 4, color: "#ef5c63", fontFamily: "verdana", fontSize: 10, userSelect: "none"})

    // create time forward button
    var timeStepForward = creative.createElement({innerHTML: "^", left: 555, top: -1, color: "#ef5c63", fontFamily: "verdana", fontWeight: "bold", fontSize: 12, cursor: "pointer", userSelect: "none"});

    // create time backward button
    var timeStepBackward = creative.createElement({innerHTML: "^", left: 555, top: 10, color: "#ef5c63", fontFamily: "verdana", fontWeight: "bold", rotation: 180, fontSize: 12, cursor: "pointer", userSelect: "none"});

    //create labels
    for (var label in tl._labels) {
      labels[i] = tl._labels[label];
      labelNames[i] = label;
      creative.createElement({background: "#0e2c31", width: 2, height: 20, left: 510 * (tl._labels[label] / tl.totalDuration()) + 4, parent: container});
      creative.createElement({innerHTML: labelNames[i], fontFamily: "verdana", fontSize: 9, left: 510 * (tl._labels[label] / tl.totalDuration()) + 9, top: 12, userSelect: "none", parent: container});
      i++;
    }

    // create scrubbing ball
    var ball = creative.createElement({id: "scrubberBall", width: 10, height: 10, borderRadius: 10, background: "#0e2c31", position: "absolute", left: 0, marginLeft: -5, cursor: "pointer", parent: container});

    //create play / pause playButton container
    var buttonContainer = creative.createElement({id: "playButton", width: 20, height: 20, position: "absolute", left: 236, top: 31});

    //create play / pause button
    var playButton = creative.createElement({width: 10, height: 0, borderRadius: 2, borderTop: "10px solid transparent", borderBottom: "10px solid transparent", borderLeft: "10px solid #ef5c63", background: "#ef5c63", cursor: "pointer", position: "absolute"});

    //create rewind button
    var rewindButton = creative.createElement({innerHTML: "|<", position: "absolute", left: 200, top: 40, fontFamily: "verdana", fontSize: "14px", letterSpacing: "-3px", margin: 0, lineHeight: 0, userSelect: "none", cursor: "pointer", color: "#ef5c63"});

    //create forward button
    var forwardButton = creative.createElement({innerHTML: ">|", position: "absolute", left: 279, top: 40, fontSize: "14px", fontFamily: "verdana", letterSpacing: "-3px", margin: 0, lineHeight: 0, userSelect: "none", cursor: "pointer", color: "#ef5c63"});

    //add to DOM
    toggleContainer.appendChild(container);
    buttonContainer.appendChild(playButton);
    toggleContainer.appendChild(buttonContainer);
    toggleContainer.appendChild(rewindButton);
    toggleContainer.appendChild(forwardButton);
    mainContainer.appendChild(name);
    toggleContainer.appendChild(time);
    toggleContainer.appendChild(timeStepForward);
    toggleContainer.appendChild(timeStepBackward);
    mainContainer.appendChild(toggleContainer);
    document.body.appendChild(mainContainer);

    var scrubbing = false;
    var dragging = false;
    var clickedName = false;
    var mousePos = 0;
    var scrubPos = 0;
    var timelinePos = 0;
    var offsetX = 0;
    var offsetY = 0;

    //click scrub bar
    container.addEventListener("mousedown", function() {
      scrubbing = true;
      setScrubPos();
    });

    //leave scrub bar
    container.addEventListener("mouseleave", function() {
      scrubbing = false;
    });

    // release scrub bar
    container.addEventListener("mouseup", function() {
      scrubbing = false;
    });

    // get mouse pos
    container.addEventListener("mousemove", function(event) {
      mousePos = event.clientX - (mainContainer.offsetLeft - mainContainer.scrollLeft);
      setScrubPos();
    });

    // set the scrubber position
    function setScrubPos() {
      //calculate new scrubber position (clamp between 10 and 510)
      scrubPos = Math.min(Math.max(mousePos, Math.min(10, 520)), Math.max(10, 520)) - 10;

      if (scrubbing) {
        ball.set({left: scrubPos});
        //set position of timeline
        tl.progress(scrubPos / 510);
      } else {
        ball.set({left: 510 * timelinePos});
      }
    }

    //play / pause
    playButton.addEventListener("click", function() {
      if (tl.isActive()) {
        tl.pause();
      } else {
        tl.play();
      }
    });

    //skip backwards
    var closestSmallerNumber = null;
    var difference = null;
    rewindButton.addEventListener("click", function() {

      for (var i = 0; i < labels.length; i++) {
        if (labels[i] < tl.time()) {

          difference = tl.time() - labels[i];

          if (tl.time() - labels[i] <= difference) {
            closestSmallerNumber = labels[i];
            difference = tl.time() - closestSmallerNumber;
          }
        }

        if (closestSmallerNumber == tl.time() || closestSmallerNumber == null) closestSmallerNumber = 0;
      }

      tl.pause();
      tl.seek(closestSmallerNumber);
    });

    //skip forward
    var closestBiggerNumber = null;
    forwardButton.addEventListener("click", function() {

      for (var i = labels.length; i >= 0; i--) {
        if (labels[i] > tl.time()) {

          difference = labels[i] - tl.time();

          if (labels[i] - tl.time() <= difference) {
            closestBiggerNumber = labels[i];
            difference = closestBiggerNumber - tl.time();
          }
        }
      }

      if (closestBiggerNumber == tl.time() || closestBiggerNumber == null) closestBiggerNumber = tl.totalDuration();

      tl.pause();
      tl.seek(closestBiggerNumber);
    });

    //seek forward 0.1 seconds
    timeStepForward.addEventListener("click", function() {
      tl.pause();
      tl.seek(tl.time() + 0.1);
    });

    //seek backward 0.1 seconds
    timeStepBackward.addEventListener("click", function() {
      tl.pause();
      tl.seek(tl.time() - 0.1);
    });

    function getClosest(number, array) {
      var closest = null;
      for (var item in array) {
        if (closest == null || Math.abs(number - closest) > Math.abs(item - number)) {
          closest = item;
        }
      }
      return closest;
    }

    //toggle controller
    name.addEventListener("click", function() {
      if (dragging) return;
      if (toggleContainer.get("height") == 60) {
        TweenLite.to(toggleContainer, 0.2, {height: 0, paddingTop: 0});
      } else {
        TweenLite.to(toggleContainer, 0.2, {height: 60, paddingTop: 5});
      }
    });

    //drag controller
    name.addEventListener("mousedown", function(event) {
      offsetX = event.clientX - mainContainer.offsetLeft;
      offsetY = event.clientY - mainContainer.offsetTop;
      clickedName = true;
    });

    //release controller
    name.addEventListener("mouseup", function() {
      clickedName = false;
      //bug prevention
      TweenLite.delayedCall(0.1, function() {
        dragging = false;
      })
    });

    //drag controller
    window.addEventListener("mousemove", function(event) {
      var mouseX = event.clientX;
      var mouseY = event.clientY;
      if (clickedName) dragging = true;

      if (dragging) {
        mainContainer.set({left: mouseX - offsetX, top: mouseY - offsetY});
      }
    });

    //update loop
    (function update() {
      // update timeline position variable
      timelinePos = tl.progress();

      // set time text
      time.set({innerHTML: Math.round(tl.time() * 10) / 10});

      //set scrubber position
      setScrubPos();

      //update play / pause button
      if (tl.isActive()) {
        playButton.set({background: "#ef5c63", x: 0});
      } else {
        playButton.set({background: "", x: 7});
      }

      requestAnimationFrame(update);
    })();
  }

  //global variables
  var scrubbers = [];
  var i = 0;

  //create scrubbers
  for (var _tl in timelines) {
    scrubbers[i] = new creative.scrubberController(timelines[_tl]);
    scrubbers[i].name.innerHTML = "|| " + _tl + " ||";
    scrubbers[i].mainContainer.set({bottom: 80 * i});
    i++;
  }
}




