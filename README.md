# Documentation Porsche AG HTML5 Creatives



## Basics
This project is based on the Node.js package manager, formerly known as  **npm**, (https://www.npmjs.com/) and **Gulp** (gulpjs, http://gulpjs.com/) as Taskrunner to develop and distribute the Porsche AG HTML5 Ads.



## How to start
Make sure that **npm**, and **gulp**, is properly installed and running on your system. Open up your terminal, navigate to your project folder and enter: 

```
npm install
```
Now all node packages and dependencies will be installed. This process may take a couple of minutes.
After finishing this process your project folder should contain a folder called **node_modules**.
Further informations: https://docs.npmjs.com/


## Compile for development
To start the development tasks, enter following command in your console of your project folder:

```
npm run dev
```
The ads will be compiled to **/build**. All necessary assets from common and the individuell ad folders will be copied to this folder, too.
Further informations:  http://gulpjs.com/

## Compile for distribution
To start the distribution tasks, enter following command in your console of your project folder:

```
npm run dist
```
The ads will be minified, uglyfied and compiled to **/dist**. All necessary assets from common and the individuell ad folders will be copied to this folder, too.


## Structure
Relevant files to customize the banners:

	|-- build
	|-- dist
	|-- src
    |-- common
	    |-- assets
    		|-- css
		    |-- images
		    |-- js
		    |-- video
    |-- billboardXXXx250
	    |-- assets
		    |-- css
		    |-- images
		    |-- js
		    |-- video
		    index.html

    

**build**

This is the target folder for the development output.

**dist**

This is the target folder for the compiled and minified banner.

**src**

This is the banner source folder for all kind of assets: 

*js, css, images, videos and fonts*

There are two different kind of folders: 
1. The **/common** package contains all base and shared assets. All these assets will be used by **EVERY** ad. Therefore, every code or file change will effect every ad! 
2. The ad source folders (e.g. **/billboard1010x250**)  contain all individuell assets und code for each single format. 


## Customization

###css (sass)###
The css is written in sass (http://sass-lang.com/)
The main code can get found in both **_creative.scss** and **_settings.scss** files (common and individual project folder).
Keep in mind, editing files in the common package will effect all ads!

###js###
The main code can be found **common/assets/js/Application.js**. 
Keep in mind, editing files in the common package will effect all ads!
If you want to add code for an individual ad, add the your code to **billboard_.../assets/js/main.js**.


## Add new ad format
If you want to add a new ad to your project you have to edit two files in the gulp structure folder.

		|-- gulp

Add the name of the ad to the an array in the config.js file.
gulp/config.js.
var bannerFolder = ['BannerName1', 'BannerName2' ];

Add the bannerConfig array to **common:assets** and  **common:vendor** gulp tasks in the
gulp/tasks/common.js.

## Upload to doubleclick studio
All ads are set up and ready for the deploy with doubleclick studio.
To maintain the folder structure after uploading to studio, create a zip from the distributed assets (index.html, css, image, js). Do not zip the  parent folder (in this case billboardXXXx250):

     |-- dist
	    |-- billboardXXXx250
	       index.html
	    	|-- css
	    	|-- images
	    	|-- js