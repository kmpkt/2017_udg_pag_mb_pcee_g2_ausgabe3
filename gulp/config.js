'use strict';

const globalConfig = require('../config/global');

var src = 'src';
var build = 'build';
var dist = 'dist';
var distZip = 'dist-zip';
var assets = '/assets';
var assetsDev = '/assets-dev';
var common = '/common';
var sassIncludePaths = [src + '/common/assets/css'];

var bannerFolder = globalConfig.folder;
// [
//                     // 'billboard_970x250_de',
//                     // 'halfpage_300x600_de',
//                     'medrect_300x250_de',
//                     'billboard_970x250_lithuania',
//                    ];
var bannerConfig = [];

bannerFolder.forEach(function(folder) {

  var s = src + '/' + folder;
  var d = build + '/' + folder;
  var dt = dist + '/' + folder;

  bannerConfig.push({

    src: s,
    dest: d,
    dist: dt,

    js: {
      src: s + assets + '/js/main.js',
      dest: d + assets + '/js',
      outputName: 'main.js'
    },
    sass: {
      src: s + assets + '/css/**/*.{sass,scss}',
      dest: d + assets + '/css'
    },
    sassinit: {
      src: s + assets + '/css/init.scss',
      // src: s + assets + '/css/init/**/*.{sass,scss}',
      dest: d + assets + '/css',
    },
    sasspolite: {
      src: s + assets + '/css/main.scss',
      // src: s + assets + '/css/polite/**/*.{sass,scss}',
      dest: d + assets + '/css'
    },
    markup: {
      src: s + '/index.html',
      dest: d
    },
    images: {
      //src: s + assets + '/images/**/*',
      src: s + assets + '/images/*',
      dest: d + assets + '/images'
    },
    sprites: {
      src: s + assets + '/sprites/*.png',
      srcpath: s + assets + '/sprites',
      destcss: s + assets + '/css',
      destsprite: d + assets + '/images'
    },
    assets: {
      src: s + assets + '/**',
      dest: d + assets
    },
    assetsDev: {
      src: s + assetsDev + '/**',
      dest: d + assetsDev
    }
  });

});

module.exports = {

  src: src,
  build: build,
  dist: dist,
  common: common,
  assets: assets,
  assetsDev: assetsDev,

  bannerConfig: bannerConfig,

  assetsConfig: {
    src: (function() {
      var a = bannerConfig.map(function(banner) {
        return banner.assets.src
      });
      a = a.concat(bannerConfig.map(function(banner) {
        return '!' + banner.assets.src + '/js/**'
      }));
      a = a.concat(bannerConfig.map(function(banner) {
        return '!' + banner.assets.src + '/css/**'
      }));
      a = a.concat(bannerConfig.map(function(banner) {
        return '!' + banner.assets.src + '/sprites'
      }));
      a = a.concat(bannerConfig.map(function(banner) {
        return '!' + banner.assets.src + '/sprites/**'
      }));
      return a;
    })(),
    dest: build
  },

  assetsDevConfig: {
    src: (function() {
      var a = bannerConfig.map(function(banner) {
        return banner.assetsDev.src
      });
      return a;
    })(),
    dest: build
  },

  browsersync: {
    server: {
      baseDir: [build, src]
      // , directory: true
    },
    ghostMode: true,
    files: [
      src + '/**/*.html',
      build + '/**',
      '!' + build + '/**/*.map',
      '!' + build + '/**/*.{png,jpg,gif}'
    ]

  },

  delete: {
    src: [build + '/**']
  },

  sass: {
    src: bannerConfig.map(function(banner) {
      return banner.sass.src
    }),
    dest: bannerConfig.map(function(banner) {
      return banner.sass.dest
    })
  },

  sassinit: {
    src: bannerConfig.map(function(banner) {
      return banner.sassinit.src
    }),
    dest: bannerConfig.map(function(banner) {
      return banner.sassinit.dest
    }),
    options: {
      outputStyle: 'nested',
      includePaths: sassIncludePaths,
      sourceMap: true
    }
  },

  markup: {
    src: bannerConfig.map(function(banner) {
      return banner.markup.src
    }),
    dest: bannerConfig.map(function(banner) {
      return banner.markup.dest
    }),
  },

  sasspolite: {
    src: bannerConfig.map(function(banner) {
      return banner.sasspolite.src
    }),
    dest: bannerConfig.map(function(banner) {
      return banner.sasspolite.dest
    }),
    options: {
      outputStyle: 'nested',
      includePaths: sassIncludePaths,
      sourceMap: true
    }
  },

  sprites: {
    src: bannerConfig.map(function(banner) {
      return banner.sprites.src
    }),
    srcpath: bannerConfig.map(function(banner) {
      return banner.sprites.srcpath
    }),
    destcss: bannerConfig.map(function(banner) {
      return banner.sprites.destcss
    }),
    destsprite: bannerConfig.map(function(banner) {
      return banner.sprites.destsprite
    })
  },

  browserify: {
    // Enable source maps
    debug: true,
    bundleConfigs: bannerConfig.map(function(banner) {
      return banner.js
    })
  },

  jshint: {
    src: src + '/**/*.js',
    excludeVendor: src + common + assets + '/js/vendor/**'
  },

  concat: {
    src: src + common + assets + '/js/vendor/**/*.js',
    dest: build + common + assets + '/js',
    dist: dist + assets + '/js'
  },

  images: {
    src: bannerConfig.map(function(banner) {
      return banner.images.src
    }),
    dest: build + assets + '/images',
    dist: dist + assets + '/images',
    imagemin: {
      optimizationLevel: 3,
      progressive: true,
      interlaced: true
    }
  },

  // videos: {
  //   src: src + assets + '/video/**/*',
  //   dest: build + assets + '/video',
  //   dist: dist + assets + '/video'
  // },

  autoprefixer: {
    def: [
      'ie >= 8',
      'ie_mob >= 9',
      'ff >= 27',
      'chrome >= 20',
      'safari >= 5',
      'opera >= 1',
      'ios >= 7',
      'android >= 3.0',
      'bb >= 10'
    ]
  },

  fonts: {
    src: src + assets + '/fonts/**/*',
    dest: build + assets + '/fonts',
    dist: dist + assets + '/fonts'
  },

  watch: {
    sass: bannerConfig.map(function(banner) {
      return banner.sass.src
    }).concat([src + common + assets + '/css/**/*.scss']),
    scripts: src + '/**/*.js',
    images: bannerConfig.map(function(banner) {
      return banner.images.src
    }),
    sprites: bannerConfig.map(function(banner) {
      return banner.sprites.src
    }),
    markup: bannerConfig.map(function(banner) {
      return banner.markup.src
    }),
  },

  distribution: {
    htmlSrc: src + '/*.*',
    imageSrc: src + assets + '/images/**/*',
    cssSrc: build + assets + '/css/*.css',
    jsSrc: build + assets + '/js/*.js',
    imageDist: dist + assets + '/images',
    cssDist: dist + assets + '/css',
    jsDist: dist + assets + '/js',
    dist: dist
  },

  zip: {
    src: dist + '/**/*',
    dist: distZip + '/',
    options: {}
  }
};
