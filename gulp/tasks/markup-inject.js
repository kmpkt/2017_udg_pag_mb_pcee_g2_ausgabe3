const gulp           = require('gulp');
const htmlreplace    = require('gulp-html-replace');
const util           = require('gulp-util');
const config         = require('../config');
const creativeConfig = require('../../' + util.env.config);


gulp.task('markup-inject', function() {
  return gulp.src(config.markup.src, { base: './src' })
    .pipe(htmlreplace({
      'video-source-tpl': {
        src: [creativeConfig.video.file],
        tpl: creativeConfig.video.tpl,
      },
      'timeline-scrubber-tpl': '<script type="text/javascript" src="../js/timeline-scrubber.js"></script>'
    }))
    .pipe(gulp.dest(config.build + '/'))
});

// gulp.task('markup-inject:dist', function() {
//   return gulp.src(config.markup.src, { base: './src' })
//     .pipe(htmlreplace({
//       'video-placeholder': {
//         src: creativeConfig.video,
//         tpl: '<source id="video_1_mp4_src_dc" type="video/mp4" src="%s"/><source id="video_1_ogg_src_dc" type="video/ogv" src="assets/videos/video.ogv"/><source id="video_1_webm_src_dc" type="video/webm" src="assets/videos/video.webm"/>'
//
//       }
//     }))
//     .pipe(gulp.dest(config.dist + '/'))
// });