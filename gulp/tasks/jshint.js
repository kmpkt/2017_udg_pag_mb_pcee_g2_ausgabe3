var gulp = require('gulp');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var config = require('../config').jshint;

/**
 * Check JavaScript sytax with JSHint
 */
gulp.task('jshint', function ()
{
    return gulp.src([config.src, '!' + config.excludeVendor])
        .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter(stylish));
});