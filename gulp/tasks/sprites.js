'use strict';

var gulp = require('gulp');
var config = require('../config');
var cache = require('gulp-cache');
var merge = require('merge-stream');
var source = require('vinyl-source-stream');
var spritesmith = require('gulp.spritesmith');
var debug = require('gulp-debug');
var handleErrors = require('../util/handleErrors');


// gulp.task('cache-clear', function(done) {
//   return cache.clearAll(done);
// });

gulp.task('sprites', function(callback) {
  var spriteData, imgStream, cssStream,
    spriteMap = config.sprites.src,
    srcPathMap = config.sprites.srcpath,
    imgMap = config.sprites.destsprite,
    cssMap = config.sprites.destcss;

  var spriteMapQueue = spriteMap.length;
  // TODO: Fix this...
  var stream = merge();

  for (var i in spriteMap)
  {
    spriteData = gulp.src(spriteMap[i], {base: config.src})
      //.pipe(debug({title: config.sprites.destcss}))
      .pipe(spritesmith({
        imgName: 'sprite.png',
        imgPath: '../images/sprite.png',
        cssName: 'polite/_sprites.scss',
        retinaSrcFilter: [srcPathMap[i] + '/*@2x.png'],
        retinaImgName: 'sprite-2x.png',
        retinaImgPath: '../images/sprite-2x.png'
      }));
    imgStream = spriteData.img
      //.pipe(imagemin())
      .pipe(gulp.dest(imgMap[i]));
    stream.add(imgStream);
    cssStream = spriteData.css
      //.pipe(csso())
      .pipe(gulp.dest(cssMap[i]));
    stream.add(cssStream);
    //test2 = merge(cssStream);
    if (spriteMapQueue)
    {
      spriteMapQueue--;
      if (spriteMapQueue === 0)
      {
        // If queue is empty, tell gulp the task is complete.
        // https://github.com/gulpjs/gulp/blob/master/docs/API.md#accept-a-callback
        //callback();
        //return stream;
      }
    }
  }
  // Return a merged stream to handle both `end` events
  return stream;
});
