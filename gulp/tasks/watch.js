'use strict';

var gulp = require('gulp');
var config = require('../config').watch;
var browserSync = require('browser-sync');

gulp.task('watch', ['browsersync'], function() {
  gulp.watch(config.sprites, ['sprites']);
  gulp.watch(config.sass, ['sassinit', 'sasspolite']);
  gulp.watch(config.scripts, ['scripts']);
  gulp.watch(config.markup, ['markup-inject']);
  // gulp.watch(config.fonts, ['fonts']);
  // gulp.watch(config.video, ['video']);
  // gulp.watch(config.markup, ['markup']);
});
