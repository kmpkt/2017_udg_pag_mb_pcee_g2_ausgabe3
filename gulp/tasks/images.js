'use strict';

var gulp = require('gulp');
var cache = require('gulp-cache');
var imagemin = require('gulp-imagemin');

var handleErrors = require('../util/handleErrors');
var config = require('../config');

//
// IMAGES
//

// gulp.task('cache-clear', function(done) {
//   return cache.clearAll(done);
// });

gulp.task('images', function() {
  console.log('====>images');
  return gulp.src(config.images.src, {
      base: './src'
    })
    .pipe(imagemin(config.images.imagemin))
    // .pipe(cache(imagemin(config.imagemin), {
    //   fileCache: new cache.Cache({
    //     tmpDir: '.gulp-cache'
    //   })
    // }))
    .pipe(gulp.dest(config.build + '/'))
    .on('error', handleErrors);
});
