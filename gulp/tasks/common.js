var gulp = require('gulp');
var config = require('../config');

var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var bannerConfig = config.bannerConfig;
var runSequence = require('run-sequence');


gulp.task('common', function(cb) {
  runSequence('common:dc', 'common:assets', 'common:vendor', cb);
});


gulp.task('common:dc', function(cb) {
  var gulpChain = gulp.src(
    config.src + config.common + config.assets + '/js/pre/*'
  ).pipe(rename({suffix: '.min'}));

  bannerConfig.forEach(function(el) {
    gulpChain = gulpChain.pipe(uglify()).pipe(gulp.dest(el.dest + '/assets/js'))
  });
  return gulpChain;
});


gulp.task('common:assets', function(cb) {
  var gulpChain = gulp.src([
    config.src + config.common + '/**',
    '!' + config.src + config.common + config.assets + '/css/**',
    '!' + config.src + config.common + config.assets + '/js/**'
  ]);

  bannerConfig.forEach(function(el) {
    gulpChain = gulpChain.pipe(gulp.dest(el.dest))
  });
  return gulpChain;
});


gulp.task('common:vendor', function(cb) {
  var gulpChain = gulp.src(config.src + config.common + config.assets + '/js/vendor/**/*.js', {base: './src/common'})
    .pipe(uglify());

  bannerConfig.forEach(function(el) {
    gulpChain = gulpChain.pipe(gulp.dest(el.dest))
  });
  return gulpChain;
});
