'use strict';

const gulp = require('gulp');
const util = require('gulp-util');
const config = require('../config');
const bannerConfig = config.bannerConfig;
const creativeConfig = require('../../' + util.env.config);
const uglify = require('gulp-uglify');
const ignore = require('gulp-ignore');
const del = require('del');
const header = require('gulp-header');
const minifyCss = require('gulp-minify-css');
const runSequence = require('run-sequence');
const rename = require('gulp-rename');
const foreach = require('gulp-foreach');
const htmlreplace = require('gulp-html-replace');
const zip = require('gulp-zip');

gulp.task('delete:dist', function(cb) {
  del(config.dist, cb);
});

gulp.task('js:dist', function() {
  return gulp.src(bannerConfig.map(function(banner) {
    return banner.js.dest + '/' + banner.js.outputName
  }), {base: './build'})
    .pipe(uglify())

    // .pipe(ignore.exclude(config.path.build + '/' + config.path.js + '/vendor/**'))
    // .pipe(header(banner, {
    //   pkg: pkg
    // }))
    .pipe(gulp.dest(config.dist));
});

gulp.task('assets:dist', function() {
  var a = bannerConfig.map(function(banner) {
    return banner.dest + '/**'
  });
  // Exclude build only test assets
  a = a.concat(bannerConfig.map(function(banner) {
    return '!' + banner.assetsDev.dest
  }));
  a = a.concat(bannerConfig.map(function(banner) {
    return '!' + banner.assetsDev.dest + '/**'
  }));
  return gulp.src(a, {base: './build'})
    .pipe(gulp.dest(config.dist));
});

gulp.task('html:dist', function() {
  // return gulp.src(bannerConfig.map(function(banner) {  return banner.src + '/**/*.html' ; }), {base: './src'})
  return gulp.src(bannerConfig.map(function(banner) {
    return banner.dest + '/**/*.html';
  }), {base: './src'})
    .pipe(gulp.dest(config.dist));
});


gulp.task('sassinit:dist', function() {
  return gulp.src(bannerConfig.map(function(banner) {
    return banner.sassinit.dest + '/**'
  }), {base: './build'})
    .pipe(minifyCss({
      rebase: false,
      advanced: false,
      keepSpecialComments: 0
      // processImport:false,
      // compatibility: 'ie8'
    }))
    .pipe(gulp.dest(config.dist));
});

gulp.task('sasspolite:dist', function() {
  return gulp.src(bannerConfig.map(function(banner) {
    return banner.sasspolite.dest + '/**'
  }), {base: './build'})
    .pipe(minifyCss({
      rebase: false,
      advanced: false,
      keepSpecialComments: 0
      // processImport:false,
      // compatibility: 'ie8'
    }))
    .pipe(gulp.dest(config.dist));
});

gulp.task('markup-inject:dist', function() {
  return gulp.src(config.markup.src, { base: './src' })
    .pipe(htmlreplace({
      'video-source-tpl': {
        src: [creativeConfig.video.file],
        tpl: creativeConfig.video.tpl,
      }
    }))
    .pipe(gulp.dest(config.dist + '/'))
});

gulp.task('zip:dist', function() {
  return gulp.src(bannerConfig.map(function(banner) {
    return banner.dist
  }))
    .pipe(foreach(function(stream, file) {
      let fileName = file.path.substr(file.path.lastIndexOf("/") + 1);
      gulp.src("./dist/" + fileName + "/**")
        .pipe(zip(fileName + ".zip"))
        .pipe(gulp.dest(config.dist));

      console.log(fileName);
      // gulp.src("./dist/"+fileName+"/**/*")
      // .pipe(zip(fileName+".zip"))
      // .pipe(gulp.dest("./zipped"));

      return stream;
    }));

    // .pipe( zip('banner.zip'))
    // .pipe(rename(function (path) {
    //   console.log(path);
    // path.basename += path.dirname+"-banner";
    // }))

    // .pipe(zip(Math.random() + 'banner.zip'))
    // .pipe(zip(banner + '.zip'))

    // .pipe(gulp.dest(config.dist));

});

/**
 * Run all tasks needed for a build in defined order
 */
gulp.task('dist', function(cb) {
  runSequence('build', 'delete:dist', 'assets:dist', [
      'html:dist',
      'js:dist',
      'sassinit:dist',
      'sasspolite:dist',
      'markup-inject:dist'
      // 'markup-inject:dist',
      // 'scripts',
      // 'assets'
      // 'fonts',
      // 'markup',
      // 'vendor'
    ],
    'zip:dist',
    //'base64',
    cb);
});
