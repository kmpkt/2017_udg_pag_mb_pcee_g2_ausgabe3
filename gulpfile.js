'use strict';

var requireDir = require('require-dir');

// Require all tasks in gulp/talks, including subfolders
requireDir('./gulp/tasks', { recurse: true });