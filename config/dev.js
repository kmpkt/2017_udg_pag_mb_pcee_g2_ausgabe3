module.exports = {
  video: {
    file: ['assets/videos/video.mp4',
      'assets/videos/video.ogv',
      'assets/videos/video.webm'],
    tpl: '<source id="video_1_mp4" type="video/mp4" src="%s"/>' +
    '<source id="video_1_ogg" type="video/ogv" src="%s"/>' +
    '<source id="video_1_webm" type="video/webm" src="%s"/>'
  }
};
