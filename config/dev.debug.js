module.exports = {
  video: {
    file: ['assets-dev/videos/video_short.mp4',
      'assets-dev/videos/video_short.ogv',
      'assets-dev/videos/video_short.webm'],
    tpl: '<source id="video_1_mp4" type="video/mp4" src="%s"/>' +
    '<source id="video_1_ogg" type="video/ogv" src="%s"/>' +
    '<source id="video_1_webm" type="video/webm" src="%s"/>'
  }
};
